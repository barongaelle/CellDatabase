//
// Created by Aline et Guillaume on 30/11/17.
//

#include <string>
#include <iostream>
#include <fstream>
#include <zconf.h>
#include <unistd.h>

#include "addCell.h"
#include "Repository.h"

#define GetCurrentDir getcwd


using namespace std;
 




void displayMenu(){
    cout << " ------------------------------------------------------------------------ " << endl;
    cout << "0.  Retour" << endl;
    cout << "1.  Ajouter une cellule à la Base de donnée" << endl;
    cout << "2.  Suppression une cellule de la base de donnée" << endl;
    cout << "3.  Afficher une cellule selon sont identifiant" << endl;
    cout << "4.  Afficher toutes les cellule Prokaryotes" << endl;
    cout << "5.  Afficher toutes les cellule Eukaryotes" << endl;
    cout << "6.  Afficher le nom de la bactérie associée (Prokaryotes) ou l’organe associé (Eucaryote) selon l’identifiant" << endl;
    cout << "7.  Afficher toutes les cellules en fonction de la taille min et max" << endl;
    cout << "8.  Afficher toutes les cellules ayant au moins un chromosome selon 1 gène" << endl;
    cout << "9.  Export d'une cellule selon sont identifiant" << endl;
    cout << "10. Export de toutes les cellule Prokaryotes" << endl;
    cout << "11. Export de toutes les cellule Eukaryotes" << endl;
    cout << "12. Export du nom de la bactérie associée (Prokaryotes) ou l’organe associé (Eucaryote) selon l’identifiant" << endl;
    cout << "13. Export de toutes les cellules en fonction de la taille min et max" << endl;
    cout << "14. Export de toutes les cellules ayant au moins un chromosome selon 1 gène" << endl;
    cout << " ------------------------------------------------------------------------ " << endl;
}

void selectMenu(Repository bdd) {

    cout << "\n\n ######################## \n" << endl;
    cout << "   Bienvenu dans votre base de données cellulaires \n" << endl;
    cout << " ######################## \n\n" << endl;

    displayMenu();

    string path = getPath();

    bool selection = true;
    int choice = 0;
    while (selection) {
        cin >> choice;

        switch (choice) {
            case 1 : { // Ajouter cellule
                generateCell(bdd);
                break;
            }

            case 2 : { // Supprimer 1 cellule selon sont identifiant
                int idCell;
                cout << "Veuillez sairir l'identifiant de la cellule à supprimer" << endl;
                cin >> idCell;
                cout << "\n" << endl;
                bdd.delCellById(idCell);
                cout << "Suppression validée" << endl;
                displayMenu();
                break;
            }

            case 3 : { // Afficher une cellule selon sont identifiant
                int id;
                cout << "Veuillez saisir l'identifiant de la cellule à afficher" << endl;
                cin >> id;
                cout << "\n" << endl;
                bdd.displayCellById(id);
                displayMenu();
                break;
            }
            
            case 4 : { // Affichier toutes les cellules Prokaryotes
                cout << "\n" << endl;
                bdd.displayOnlyProkaryote();
                displayMenu();
                break;
            }

            case 5 : { // Afficher toutes les cellules Eukaryotes
                cout << "\n" << endl;
                bdd.displayOnlyEukaryote();
                displayMenu();
                break;
            }


            case 6 : { //Afficher le nom de la bactérie associée (Prokaryotes) ou l’organe associé (Eucaryote) selon l’identifiant
                int id;
                cout << "Veuillez saisir l'identifiant de la cellule : trouver bactérie ou organe " << endl;
                cin >> id;
                cout << "\n" << endl;
                bdd.displayEukaryoteOrganeById(id);
                displayMenu();
                break;
            }    

            case 7 : { // Afficher cellules conmprise entre 2 tailles (micromètre)
                double min;
                double max;
                cout << "Veuillez entrer la taille minimum (micromètre)" << endl;
                cin >> min;
                cout << "Veuillez entrer la taille maximum (micromètre)" << endl;
                cin >> max;
                cout << "\n" << endl;
                bdd.displayCellSizeBetween(min, max);
                displayMenu();
                break;
            }

            case 8 : { // Afficher toutes les cellules ayant au moins un chromosome selon 1 gène
                cout << "\n" << endl;
                string nameGene;
                cout << "Veuillez entrer le nom du gène recherché" << endl;
                cin >> nameGene;
                cout << "\n" << endl;
                bdd.displayCellHavingChromosomeExprimedGene(nameGene);
                displayMenu();
                break;
            }


            case 9 : { // Export 1 cellules de la base selon id
                int id;
                cout << "Veuillez saisir l'identifiant de la cellule à exporter" << endl;
                cin >> id;
                cout << "\n" << endl;

                bdd.ExportCellById(id);
                displayMenu();
                break;
            }

            case 10 : { // Export de toutes les cellule Prokaryotes 
                cout << "\n" << endl;
                bdd.exportOnlyProkaryote();
                displayMenu();
                break;

            }

            case 11 : { // Export de toutes les cellule Eukaryote 
                cout << "\n" << endl;
                bdd.exportOnlyEukaryote();
                displayMenu();
                break;
            }

            case 12 : { // Export du nom de la bactérie associée (Prokaryotes) ou l’organe associé (Eucaryote) selon l’identifiant
                int id;
                cout << "Veuillez saisir l'identifiant de la cellule à exporter" << endl;
                cin >> id;
                cout << "\n" << endl;
                bdd.exportEukaryoteOrganeById(id);           
                displayMenu();
                break;

            }           
            case 13 : { // Export de toutes les cellules en fonction de la taille min et max
                double min;
                double max;
                cout << "Veuillez entrer la taille minimum (micromètre)" << endl;
                cin >> min;
                cout << "Veuillez entrer la taille maximum (micromètre)" << endl;
                cin >> max;
                cout << "\n" << endl;
                bdd.exportCellSizeBetween(min, max);
                displayMenu();
                break;
            }  
            case 14 : { // Export de toutes les cellules ayant au moins un chromosome selon 1 gène
                cout << "\n" << endl;
                string nameGene;
                cout << "Veuillez entrer le nom du gène recherché" << endl;
                cin >> nameGene;
                cout << "\n" << endl;
                bdd.exportCellHavingChromosomeExprimedGene(nameGene);
                displayMenu();
                break;
            }  

            case 0: {
                cout << "\nMay the force be with you \n\n" << endl;
                exit(0);
            }

            default :
                cout << "Erreur de saisi" << endl;
                displayMenu();
                cout << "Que voulez vous faire?" << endl;
                break;
        }
    }
}

string getPath(){
    char buff[FILENAME_MAX];
    GetCurrentDir( buff, FILENAME_MAX );
    string stringPath(buff);
    return stringPath;
}

