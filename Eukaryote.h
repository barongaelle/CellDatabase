//
// Created by Aline et Guillaume on 16/11/17.
//

#pragma once

#include <iostream>
#include <list>

using namespace std;

#include "Cell.h"
#include "Chromosome.h"


class Eukaryote : public Cell {

private:
    string organeName;
    list<Chromosome> chromoList;

public:

    /** 
     * Constructor
     * @param id
     * @param size
     * @param nameOrgane
     */
    Eukaryote(int id = 0, double size = 0, string organeName = "");


    /**
     * Get name
     * @return string
     */
    string getOrganeName() const;


    /**
     * Set name
     * @param name
     */
    void setOrganeName(string name);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream & out) const;


    /**
     * Get array chromosome
     * @return list<Chromosome>
     */
    list<Chromosome> getChromosomeList() const;


    /**
     * Set arrayKro
     * @param arrayKro
     */
    void setChromosomeList(list<Chromosome> chromoList);


    /**
     * Add Chromosome
     * @param kro
     */
    void addChromosome(Chromosome chromo);


    /**
     * Override operator ==
     * @param euk
     * @return bool
     */
    bool operator==(Eukaryote euka) const;


    /**
     * Copy Constructor
     * @param euk
     */
    Eukaryote(const Eukaryote& euka);

};


/**
 * Override operator <<
 * @param out
 * @param c
 * @return
 */
ostream & operator<<(ostream &out, const Eukaryote &e);


