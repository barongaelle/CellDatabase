//
// Created by visenya on 16/11/17.
//

#include "Prokaryote.h"

//Constructor

Prokaryote::Prokaryote(int id, double size, string bacteriumName, Chromosome chromosome): Cell(id, size), chro(chromosome){
    this->type = "Prokaryote";
};


// Getter and Setter

string Prokaryote::getBacteriumName() const {
    return this->bacteriumName;
}

Chromosome Prokaryote::getChromosome() const {
    return this->chro;
}

void Prokaryote::setChromosome(Chromosome chromo) {
    this->chro = chromo;
}

void Prokaryote::setBacteriumName(string bacteriumName) {
    this->bacteriumName = bacteriumName;
}
 

// Display

void Prokaryote::affiche(ostream & out) const {
    out << "**********************************************************************\n" << endl;
    out << "Cellules Prokaryote " << endl;
    out << " - Nom de la bactérie = " << endl;
    Cell::affiche(out);
    
    out << this->bacteriumName << endl;
    if (this->chro.getChromosomeName() != "") {
        out << this->chro << endl;
    }
    out << "\n**********************************************************************" << endl;

}


// Override operator
ostream & operator<<(ostream &out, const Prokaryote &p){
    p.affiche(out);
    return out;
}


bool Prokaryote::operator==(Prokaryote proka) const {
    return (this->id == proka.id) && (this->size == proka.size) && (this->bacteriumName == proka.bacteriumName) && (this->chro == proka.chro);
}




