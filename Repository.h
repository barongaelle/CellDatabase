//
// Created by Aline et Guillaume on 26/11/17.
//

#pragma once

#include <string>
#include <iostream>
#include <list>
#include <algorithm>
#include <fstream>
using namespace std;


#include "Eukaryote.h"
#include "Prokaryote.h"
#include "Cell.h"


class Repository {

private:
    list<Eukaryote> eukaryoteList;
    list<Prokaryote> prokaryoteList;

public:

    /**
     * Get array Eukaryote
     * @return list<Eukaryote>
     */
    list<Eukaryote> getEukaryoteList() const ;


    /**
     * Set array Eukaryote
     * @param arrayEukaryote
     */
    void setEukaryoteList(list<Eukaryote> eukaryoteList);


    /**
     * Get array Prokaryote
     * @return list<Prokaryote>
     */
    list<Prokaryote> getProkaryoteList() const ;


    /**
     * Set array Prokaryote
     * @param arrayProkaryote
     */
    void setProkaryoteList(list<Prokaryote> prokaryoteList);


    /**
     * Add Eukaryote
     * @param euk
     */
    void addEukaryote(Eukaryote euka);


    /**
     * Add Prokaryote by id
     * @param pro
     */
    void addProkaryote(Prokaryote pro);


    /**
     * Delete Eukaryote by id
     * @param id
     */
    void delEukaryoteById(int id);


    /**
     * Delete Prokaryote
     * @param id
     */
    void delProkaryoteById(int id);


    /**
     * Delete a cell by id
     * @param id
     */
    void delCellById(int id);


    /**
    * Display
    * @param out
    */
    virtual void affiche(ostream & out) const;

    // Fct with filter


    /**
     * Display Cell by Id
     * @param id
     */
    void displayCellById(int id);

    /**
     * Display Prokaryote by Id
     * @param id
     */
    void displayProkaryoteById(int id);

    /**
     * Display Eukaryote by Id
     * @param id
     */
    void displayEukaryoteById(int id);

    /**
     * Export Eukaryote by Id
     * @param id
     */
    void exportEukaryoteByID(int id);


    void exportEukaryoteOrganeById(int id);

    /**
     * Display all Prokaryote
     */
    void displayOnlyProkaryote();
    void exportOnlyProkaryote();
    void exportOnlyEukaryote();

    void ExportCellById(int id);
    void ExportProkaryoteById(int id);
    void ExportEukaryoteById(int id);
    /**
     * Display all Eukaryote
     */
    void displayOnlyEukaryote();

    void exportCellSizeBetween(double min, double max);
    void exportProkaryoteSizeBetween(double min, double max);
    void exportEukaryoteSizeBetween(double min, double max);

    /**
     * Display all Cell between a defined size
     * @param min
     * @param max
     */
    void displayCellSizeBetween(double min, double max);

    /**
     * Display Eukaryote between a defined size
     * @param min
     * @param max
     */
    void displayEukaryoteSizeBetween(double min, double max);

    /**
     * Display Prokaryote between a defined size
     * @param min
     * @param max
     */
    void displayProkaryoteSizeBetween(double min, double max);



    /**
     * Display Eukaryote Organe by ID
     * @param id
     */
    void displayEukaryoteOrganeById(int id);


    /**
     * Display Prokaryote bactérium by ID
     * @param id
     */
    void displayProkaryoteBacteriumById(int id);

    /**
     * Display all Cell having a chromosome with a defined gene
     * @param idGene
     */
    void displayCellHavingChromosomeExprimedGene(string nameGene);

    /**
     * Display all Prokaryote having a chromosome with a defined gene
     * @param idGene
     */
    void displayProkaryoteHavingChromosomeExprimedGene(string nameGene);

    /**
     * Display all Eukaryote having a chromosome with a defined gene
     * @param idGene
     */
    void displayEukaryoteHavingChromosomeExprimedGene(string nameGene);



    /**
     * Display all Cell sort by size
     */
    void displayCellBySize();

    void exportCellHavingChromosomeExprimedGene(string nameGene);
    void exporEukaryoteHavingChromosomeExprimedGene(string nameGene);
    void exportProkaryoteHavingChromosomeExprimedGene(string nameGene);


};


/**
 * Override operator <<
 * @param out
 * @param d 
 * @return
 */
ostream & operator<<(ostream &out, const Repository &d);

