//
// Created by Aline et Guillaume on 16/11/17.
//
 
#pragma once

#include <iostream>
#include <list>



using namespace std;

#include "Cell.h"
#include "Gene.h"

class Chromosome {

private:
    string chromosomeName;
    string type;
    list<Gene> geneList;

public:

    /**
     * Constructor
     * @param nameChromosome
     * @param type
     */
    Chromosome(string chromosomeName = "", string type = "");


    /**
     * Get Name
     * @return name
     */
    string getChromosomeName() const;


    /**
     * Get Type
     * @return type
     */
    string getType() const;


    /**
     * Get array Gene
     * @return array of gene
     */
    list<Gene> getGeneList() const ;

    /**
     * Set Type
     * @param type
     */
    void setType(string type);


    /**
     * Set Name
     * @param nameChromosome
     */
    void setChromosomeName(string chromosomeName);


    /**
     * Set array Gene
     * @param arrayGene
     */
    void setGeneList(list<Gene> geneList);


    /**
     * Add Gene
     * @param gene
     */
    void addGene(Gene gene);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream & out) const;


    /**
     * Override operator ==
     * @param kro
     * @return boolean
     */
    bool operator==(Chromosome chromo) const;


    /**
     * Copy Constructor
     * @param kro
     */
    Chromosome(const Chromosome& chromo);

};


/**
 * Override operator <<
 * @param out
 * @param c
 * @return
 */
ostream & operator<<(ostream &out, const Chromosome &c);


