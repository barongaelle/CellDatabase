//
// Created by Baron Gaëlle and Daniaud Thomas on 23/12/2017
//

#include "Repository.h"
using namespace std;



/* Getter and Setter */

list<Eukaryote> Repository::getEukaryoteList() const {
    return this->eukaryoteList;
}

void Repository::setEukaryoteList(list<Eukaryote> eukaryoteList) {
    this->eukaryoteList = eukaryoteList;
}

list<Prokaryote> Repository::getProkaryoteList() const {
    return this->prokaryoteList;
}

void Repository::setProkaryoteList(list<Prokaryote> prokaryoteList) {
    this->prokaryoteList = prokaryoteList;
}


/* Fonction Add and Del */

void Repository::addEukaryote(Eukaryote e) {
    this->eukaryoteList.push_back(e);
}

void Repository::addProkaryote(Prokaryote p) {
    this->prokaryoteList.push_back(p);
}

void Repository::delEukaryoteById(int id) {
    if ( !this->eukaryoteList.empty() /*&& existCell*/) {
        list<Eukaryote>::iterator it = eukaryoteList.begin();
        while (it != this->eukaryoteList.end()){
            if (it->getIdName() == id){
                this->eukaryoteList.erase(it);
                break;
            }
            it++;
        }
    }
}

void Repository::delProkaryoteById(int id) {
    if ( !this->prokaryoteList.empty() /*&& existCell*/) {
        list<Prokaryote>::iterator it = prokaryoteList.begin();
        while (it != this->prokaryoteList.end()){
            if (it->getIdName() == id){
                this->prokaryoteList.erase(it);
                break;
            }
            it++;
        }
    }
}

void Repository::delCellById(int id) {
    delProkaryoteById(id);
    delEukaryoteById(id);
}



/* Fonction with filter */

void Repository::displayCellById(int id){
    displayProkaryoteById(id);
    displayEukaryoteById(id);
}

void Repository::ExportCellById(int id){
    ExportProkaryoteById(id);
    ExportEukaryoteById(id);
}

void Repository::ExportProkaryoteById(int id){

    ofstream fichier("All_Proka_database.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
             
    if ( !this->prokaryoteList.empty()) {
        list<Prokaryote>::iterator itProkaryote = prokaryoteList.begin();
        while (itProkaryote != this->prokaryoteList.end()){
            if (itProkaryote->getIdName() == id){
                if(fichier){
                    fichier << *itProkaryote << endl;
                    fichier.close();
                }else
                    cerr << "Impossible d'ouvrir le fichier !" << endl;
                break;
            }
            itProkaryote++;
        }
    }
}


void Repository::ExportEukaryoteById(int id){
    ofstream fichier("All_Euka_database.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();
        while (itEukaryote != this->eukaryoteList.end()){
            if (itEukaryote->getIdName() == id) {
                if(fichier){
                    fichier << *itEukaryote << endl;
                    fichier.close();
                }else
                    cerr << "Impossible d'ouvrir le fichier !" << endl;
                break;
            }
            itEukaryote++;
        }
    }
}


void Repository::displayEukaryoteById(int id){
    if ( !this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();
        while (itEukaryote != this->eukaryoteList.end()){
            if (itEukaryote->getIdName() == id) {
                cout << *itEukaryote << endl;
                break;
            }
            itEukaryote++;
        }
    }
}


void Repository::displayEukaryoteOrganeById(int id) {
    if ( !this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();
        while (itEukaryote != this->eukaryoteList.end()){
            if (itEukaryote->getIdName() == id) {
                cout << itEukaryote->getOrganeName() << endl;
                break;
            }
            itEukaryote++;
        }
    }
}

void Repository::exportEukaryoteOrganeById(int id) {
    ofstream fichier("organe.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();
        while (itEukaryote != this->eukaryoteList.end()){
            if (itEukaryote->getIdName() == id) {
                if(fichier){
                    fichier << itEukaryote->getOrganeName() << endl;
                    fichier.close();
                }else
                    cerr << "Impossible d'ouvrir le fichier !" << endl;
                break;
            }
            itEukaryote++;
        }
    }
}


void Repository::exportEukaryoteByID(int id){
    ofstream fichier("test.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert
             
    if ( !this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();
        while (itEukaryote != this->eukaryoteList.end()){
            if (itEukaryote->getIdName() == id) {
                if(fichier){
                    fichier << "La cellule d'identifiant :" <<id << "appartient :" << endl;
                    fichier << *itEukaryote << endl;
                    fichier.close();
                }else
                    cerr << "Impossible d'ouvrir le fichier !" << endl;
                break;
            }
            itEukaryote++;
        }
    }
}




void Repository::displayProkaryoteById(int id){
    if ( !this->prokaryoteList.empty()) {
        list<Prokaryote>::iterator itProkaryote = prokaryoteList.begin();
        while (itProkaryote != this->prokaryoteList.end()){
            if (itProkaryote->getIdName() == id){
                cout << *itProkaryote << endl;
                break;
            }
            itProkaryote++;
        }
    }
}

void Repository::displayProkaryoteBacteriumById(int id) {
    if ( !this->prokaryoteList.empty()) {
        list<Prokaryote>::iterator itProkaryote = prokaryoteList.begin();
        while (itProkaryote != this->prokaryoteList.end()){
            if (itProkaryote->getIdName() == id) {
                cout << itProkaryote->getBacteriumName() << endl;
                break;
            }
            itProkaryote++;
        }
    }
}


void Repository::displayOnlyEukaryote() {
    if ( !this->eukaryoteList.empty() ) {
        list<Eukaryote>::iterator it = eukaryoteList.begin();
        while (it != this->eukaryoteList.end()){
            cout << *it << endl;
            it++;
        }
    }
}

void Repository::exportOnlyEukaryote() {
    ofstream fichier("Only_Euka.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->eukaryoteList.empty() ) {
        list<Eukaryote>::iterator it = eukaryoteList.begin();
        
        if(fichier){
            while (it != this->eukaryoteList.end()){
                fichier << *it << endl;
                it++;
            }
            fichier.close();
        }else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
}

void Repository::displayOnlyProkaryote() {
    if ( !this->prokaryoteList.empty() /*&& existCell*/) {
        list<Prokaryote>::iterator it = prokaryoteList.begin();
        while (it != this->prokaryoteList.end()){
            cout << *it << endl;
            it++;
        }
    }
}


void Repository::exportOnlyProkaryote() {
    ofstream fichier("Only_Proka.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->prokaryoteList.empty() /*&& existCell*/) {
        list<Prokaryote>::iterator it = prokaryoteList.begin();
        
        if(fichier){
            while (it != this->prokaryoteList.end()){
                fichier << *it << endl;
                it++;
            }
            fichier.close();
        }else
             cerr << "Impossible d'ouvrir le fichier !" << endl;    
    }
}

void Repository::displayCellSizeBetween(double min, double max) {
    displayProkaryoteSizeBetween(min, max);
    displayEukaryoteSizeBetween(min, max);
}

void Repository::exportCellSizeBetween(double min, double max) {
    exportProkaryoteSizeBetween(min, max);
    exportEukaryoteSizeBetween(min, max);
}

void Repository::displayProkaryoteSizeBetween(double min, double max) {
    if ( !this->prokaryoteList.empty()) {
        list<Prokaryote>::iterator itProkaryote = prokaryoteList.begin();

        while (itProkaryote != this->prokaryoteList.end()){
            if (itProkaryote->getSize() >= min && itProkaryote->getSize() <= max){
                cout << *itProkaryote << endl;
            }
            itProkaryote++;
        }
    }
}

void Repository::exportProkaryoteSizeBetween(double min, double max) {
    ofstream fichier("compa_Proka_min_max.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->prokaryoteList.empty()) {
        list<Prokaryote>::iterator itProkaryote = prokaryoteList.begin();

        if(fichier){
            while (itProkaryote != this->prokaryoteList.end()){
                if (itProkaryote->getSize() >= min && itProkaryote->getSize() <= max){
                    fichier << *itProkaryote << endl;
                }
                itProkaryote++;
            }
            fichier.close();
        }else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
}

void Repository::displayEukaryoteSizeBetween(double min, double max) {
    if (!this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();

        while (itEukaryote != this->eukaryoteList.end()){
            if (itEukaryote->getSize() >= min && itEukaryote->getSize() <= max) {
                cout << *itEukaryote << endl;
            }
            itEukaryote++;
        }
    }
}

void Repository::exportEukaryoteSizeBetween(double min, double max) {   
    ofstream fichier("compa_Euka_min_max.txt", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if (!this->eukaryoteList.empty()) {
        list<Eukaryote>::iterator itEukaryote = eukaryoteList.begin();

        if(fichier){
            while (itEukaryote != this->eukaryoteList.end()){
                if (itEukaryote->getSize() >= min && itEukaryote->getSize() <= max) {
                    fichier << *itEukaryote << endl;
                }
                itEukaryote++;
            }
            fichier.close();
        }else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
}


void Repository::displayProkaryoteHavingChromosomeExprimedGene(string nameGene) {
    if ( !this->prokaryoteList.empty()) {
        for ( auto const& pro : prokaryoteList){
            if (pro.getChromosome().getChromosomeName() == "") {
            } else{
                for ( auto const& gene : pro.getChromosome().getGeneList()){
                    if (gene.getGeneName() == nameGene){
                        cout << pro << endl;
                    }
                }
            }
        }
    }
}

void Repository::displayEukaryoteHavingChromosomeExprimedGene(string nameGene) {
    if ( !this->eukaryoteList.empty()) {
        for ( auto const& euka : eukaryoteList){
            if (!euka.getChromosomeList().empty()) {
                for ( auto const& chromo : euka.getChromosomeList()){
                    if (!chromo.getGeneList().empty() ){
                        for ( auto const& gene : chromo.getGeneList()){
                            if (gene.getGeneName() == nameGene){
                                cout << euka << endl;
                            }
                        }
                    }
                }
            }
        }
    }
}


void Repository::displayCellHavingChromosomeExprimedGene(string nameGene) {
    displayProkaryoteHavingChromosomeExprimedGene(nameGene);
    displayEukaryoteHavingChromosomeExprimedGene(nameGene);
}








void Repository::exportProkaryoteHavingChromosomeExprimedGene(string nameGene) {
    ofstream fichier("ProkaryoteHavingChromosomeExprimedGene", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->prokaryoteList.empty()) {
        if(fichier){
            for ( auto const& pro : prokaryoteList){
                if (pro.getChromosome().getChromosomeName() == "") {
                } else{
                    for ( auto const& gene : pro.getChromosome().getGeneList()){
                        if (gene.getGeneName() == nameGene){
                            fichier << pro << endl;
                        }
                    }
                }
            }
            fichier.close();
        }else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
}

void Repository::exporEukaryoteHavingChromosomeExprimedGene(string nameGene) {
    ofstream fichier("EukaryoteHavingChromosomeExprimedGene", ios::out | ios::trunc);  // ouverture en écriture avec effacement du fichier ouvert

    if ( !this->eukaryoteList.empty()) {
        if(fichier){
            for ( auto const& euka : eukaryoteList){
                if (!euka.getChromosomeList().empty()) {
                    for ( auto const& chromo : euka.getChromosomeList()){
                        if (!chromo.getGeneList().empty() ){
                            for ( auto const& gene : chromo.getGeneList()){
                                if (gene.getGeneName() == nameGene){
                                    fichier << euka << endl;
                                }
                            }
                        }
                    }
                }
            }
            fichier.close();
        }else
            cerr << "Impossible d'ouvrir le fichier !" << endl;
    }
}


void Repository::exportCellHavingChromosomeExprimedGene(string nameGene) {
    exporEukaryoteHavingChromosomeExprimedGene(nameGene);
    exportProkaryoteHavingChromosomeExprimedGene(nameGene);
}









bool compCells(Cell* cell, Cell* cellBis) {
    return cell->getSize() < cellBis->getSize();
}


void Repository::displayCellBySize() {
    list<Cell*> cellList;

    if ( !this->prokaryoteList.empty()) {
        for (auto &pro : prokaryoteList) {
            cellList.push_back(&pro);
        }
    }

    if ( !this->eukaryoteList.empty()) {
        for (auto &euk : eukaryoteList) {
            cellList.push_back(&euk);
        }
    }

    cellList.sort(compCells);
    if ( !cellList.empty()) {
        for (auto const &cell : cellList) {
            cout << *cell << endl;
        }
    }

    cellList.clear();
}

void Repository::affiche(ostream & out) const {
    out << " ---- Base de données ----- " << endl;
    if (!this->eukaryoteList.empty()){
        for ( auto const& a : eukaryoteList)
            out << a;
    }
}


// Override 

ostream & operator<<(ostream &out, const Repository &c){
    c.affiche(out);
    return out;
}


