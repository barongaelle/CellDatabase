//
// Created by Aline et Guillaume on 02/12/17.
//

#pragma once

#include <string>
#include <iostream>
#include "Repository.h"

#include "addCell.h"

using namespace std;
 

/**
 * Display the menu
 */
void displayMenu();

/**
 * Creation of the menu
 * @param bdd
 */
void selectMenu(Repository bdd);

/**
 * Get path app's execution
 * @return
 */
string getPath();



