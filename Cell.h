//
// Created by Aline et Guillaume on 09/11/17.
// 

#pragma once

#include <string>
#include <iostream>
#include <list>
#include "Protein.h"


using namespace std;

class Cell {

protected:
    int id;
    double size;
    list<Protein> proteinList;
    string type;

public:

    /**
     * Constructor
     * @param id
     * @param size
     */
    Cell(int id = 0, double size = 0);


    /**
     * Get Id Cell
     * @return idName
     */
    int getIdName();


    /**
     * Get size Cell
     * @return size
     */
    double getSize() const;


    /**
     * Set IdName
     * @param IdName
     */
    void setIdName(int idName);


    /**
     * Set size
     * @param size
     */
    void setSize(double size);


    /**
     * Set Array Protein
     * @param proteinList
     */
    void setProteinList(list<Protein> proteinList);


    /**
     * Get size Cell
     * @return arrayProtein
     */
    list<Protein> getProteinList() const;


    /**
     * Display Cell
     */
    virtual void affiche(ostream &) const;


    /**
     * Add protein
     * @param p
     */
    void addProtein(Protein p);


    /**
     * Get Type
     * @return type
     */
    string getType() const;


    /**
     * Copy Constructor
     * @param cell
     */
    Cell(const Cell& cell);

};


/**
 * Redefinition of << (display)
 * @param out
 * @param c
 * @return
 */
ostream & operator<<(ostream &out, const Cell &c);



