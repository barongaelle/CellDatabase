**********************************************************************

 Cellules Eukaryote
 - Id = 1.
 - Size = 2.
 - Liste de Proteines: 
	 - Protéine : 1.
 - Nom d'organes = cerveau.
 - Liste de chromosome : 
	 - Chromosome : Name = chrs1 , Type = Circulaire.
		 - Liste de gènes : 
			 - Gene : Nom du gène = 2p22, Locus = 23.
			 - Gene : Nom du gène = 4p45, Locus = 3456.

**********************************************************************

**********************************************************************

 Cellules Eukaryote
 - Id = 3.
 - Size = 34.
 - Liste de Proteines: 
	 - Protéine : 2.
 - Nom d'organes = peua.
 - Liste de chromosome : 
	 - Chromosome : Name = chrs3 , Type = Linéaire.
		 - Liste de gènes : 
			 - Gene : Nom du gène = 0p43, Locus = 1.

**********************************************************************

