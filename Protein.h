//
// Created by Aline et Guillaume on 09/11/17.
//

#pragma once

#include <string>
#include <iostream>


using namespace std;


class Protein {

private :

    string id;
    string name;

public:
 
    /**
     * Constructor
     * @param id
     * @param name
     */
    Protein(string id = "", string name = "");


    /**
     * Get id
     * @return string
     */
    string getIdProtein() const;


    /**
     * Set id
     * @param id
     */
    void setIdProtein(string id);


    /**
     * Get Name
     * @return string
     */
    string getProteinName() const;


    /**
     * Set name
     * @param name
     */
    void setProteinName(string name);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream &out) const;

    /**
     * Override operator ==
     * @param prot
     * @return bool
     */
    bool operator==(Protein prot) const;


    /**
     * Copy Constructor
     * @param prot
     */
    Protein(const Protein& prot) : id(prot.id), name(prot.name){};

};

/**
 * Override operator <<
 * @param out
 * @param p
 * @return
 */
ostream & operator<<(ostream &out, const Protein &p);


