//
// Created by Aline et Guillaume on 16/11/17.
//
#pragma once
#include <iostream>

using namespace std;



#include "Cell.h"
#include "Chromosome.h"

class Prokaryote : public Cell {

private:
    string bacteriumName;
    Chromosome chro;

public:

    /** 
     * Constructor
     * @param id
     * @param size
     * @param chromosome
     */
    Prokaryote(int id = 0, double size = 0, string bacteriumName = "tto", Chromosome chromosome = Chromosome());


    /**
     * Get name
     * @return string
     */
    string getBacteriumName() const;


    /**
     * Set name
     * @param name
     */
    void setBacteriumName(string bname);

    /**
     * Get Chromosome
     * @return
     */
    Chromosome getChromosome() const;


    /**
     * Set Chromosome
     * @param kro
     */
    void setChromosome(Chromosome chromo);


    /**
     * Display
     * @param out
     */
    virtual void affiche(ostream &out) const;


    /**
     * Override operator ==
     * @param pro
     * @return
     */
    bool operator==(Prokaryote proka) const;

};


/**
 * Override operator <<
 * @param out
 * @param p
 * @return
 */
ostream & operator<<(ostream &out, const Prokaryote &p);


