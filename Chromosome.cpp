//
// Created by visenya on 16/11/17.
//
 
#include "Chromosome.h"

//Constructor

Chromosome::Chromosome(string chromosomeName, string type) {
    this->chromosomeName = chromosomeName;
    this->type = type;
}


// Getter and Setter

string Chromosome::getChromosomeName() const {
    return this->chromosomeName;
}

string Chromosome::getType() const {
    return  this->type;
}

list<Gene> Chromosome::getGeneList() const {
    return this->geneList;
}

void Chromosome::setType(string type) {
    this->type = type;
}

void Chromosome::setChromosomeName(string chromosomeName) {
    this->chromosomeName = chromosomeName;
}

void Chromosome::setGeneList(list<Gene> geneList) {
    this->geneList = geneList;
}


// Function

void Chromosome::addGene(Gene g) {
    this->geneList.push_back(g);
}


// Display

void Chromosome::affiche(ostream & out) const {
    out << " - Chromosome : Name = " << this->chromosomeName << " , Type = " << this->type << "." << endl;
    if (!this->geneList.empty()){
        out << "\t\t - Liste de gènes : " << endl;
        for ( auto const& a : geneList)
            out << "\t\t\t - " << a;
    }
}


// Copy Constructor

Chromosome::Chromosome(const Chromosome& chromo){
    this->chromosomeName = chromo.chromosomeName;
    this->type = chromo.type;
    this->geneList = chromo.geneList;
};


// Override operator

bool Chromosome::operator==(Chromosome chromo) const {
    return (this->chromosomeName == chromo.chromosomeName) && (this->type == chromo.type) && (this->geneList == chromo.geneList);
}

ostream & operator<<(ostream &out, const Chromosome &c){
    c.affiche(out);
    return out;
}


