//
// Created by Aline et Guillaume on 13/12/17.
//

#include "addCell.h"


Protein createProtein(){
    string idProt;
    string protName;

    cout << "Veuillez entrer l'id de la protéine" << endl;
    cin >> idProt;

    cout << "Veuillez entrer l'id du gène" << endl;
    cin >> protName;

    Protein prot = Protein(idProt, protName);

    return prot;
}

Gene createGene(){
    string geneName;
    string locus;

    cout << "Veuillez entrer le nom du gène" << endl;
    cin >> geneName;

    cout << "Veuillez entrer le locus du gène?" << endl;
    cin >> locus;

    Gene myGene(geneName, locus);

    return myGene;
}

Chromosome createChromosome(){
    string chroName;
    string type;
    int chroType;
    int nbGene = 0;

    cout << "Veuillez entrer le nom du chromosome" << endl;
    cin >> chroName;

    cout << "Quel est le type de chromosome (0 = Circulaire; 1 = Linéaire)?" << endl;
    cin >> chroType;

    if(chroType == 1) {
        type = "Linéaire";
    } else {
        type = "Circulaire";
    }

    Chromosome chro = Chromosome(chroName, type);

    cout << "Combien de gènes composent ce chromosome?" << endl;
    cin >> nbGene;

    while (nbGene > 0) {
        Gene gene = createGene();
        chro.addGene(gene);
        nbGene--;
    }

    return chro;
}

Eukaryote createEukaryote(){
    int idCell;
    double sizeCell = 0.0;
    string organeName;
    int nbProt = 0;
    int nbChro = 0;

    cout << "Merci de respecter les choix de ces questions." << endl;

    cout << "Veuillez entrer l'id de la cellule." << endl;
    cin >> idCell;

    cout << "veuillez entrer la taille de cette cellule en micromètre." << endl;
    cin >> sizeCell;

    cout << "veuillez entrer le nom de l'organe de localisation de cette cellule." << endl;
    cin >> organeName;

    // Todo : Refactoriser Fonction
    cout << "Combien de protéines composent cette cellule?" << endl;
    cin >> nbProt;

    Eukaryote euka(idCell, sizeCell, organeName);

    while (nbProt > 0) {
        Protein prot = createProtein();
        euka.addProtein(prot);
        nbProt--;
    }

    cout << "combien de chromosome composent la cellule?" << endl;
    cin >> nbChro;

    while (nbChro > 0) {
        Chromosome chromo = createChromosome();
        euka.addChromosome(chromo);
        nbChro--;
    }

    return euka;
}

Prokaryote createProkaryote(){
    int idCell;
    double sizeCell;
    string bacteriumName;
    string chromosomeName;
    int nbProt = 0;

    cout << "Merci de respecter les choix de ces questions." << endl;

    cout << "Veuillez entrer l'id de la cellule." << endl;
    cin >> idCell;

    cout << "veuillez entrer la taille de cette cellule en micromètre." << endl;
    cin >> sizeCell;

    cout << "veuillez entrer le nom de la bactérie." << endl;
    cin >> bacteriumName;

    Chromosome kro = createChromosome();

    cout << "Combien de protéines composent cette cellule?? " << endl;
    cin >> nbProt;

    Prokaryote proka(idCell, sizeCell, bacteriumName, kro);

    while (nbProt > 0) {
        Protein prot = createProtein();
        proka.addProtein(prot);
        nbProt--;
    }

    return proka;
}



void generateCell(Repository bdd){
    char answer;
    do{
        cout << "Voulez-vous enregistrer un Eukaryote (E) ou un Prokaryote (P) sinon retournez au menu principal (R)? [E/P/R]" << endl;
        cin >> answer;

        // Security
        if (cin.fail()) {  // not a valid character
            cin.clear();   // clear buffer
            cin.ignore(1024, '\n');  // repair the stream
        }

        if (answer == 'e' || answer == 'E') {
            Eukaryote euka = createEukaryote();
            bdd.addEukaryote(euka);
        } else {
            if (answer == 'p' || answer == 'P') {
                Prokaryote proka = createProkaryote();
                bdd.addProkaryote(proka);
            } else {
                selectMenu(bdd);
            }
        }

    } while (cin.fail() || answer != 'e' || answer != 'E' || answer != 'p' || answer != 'P' || answer != 'r' || answer != 'R');

}

