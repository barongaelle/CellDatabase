//
// Created by visenya on 16/11/17.
//

#include "Eukaryote.h"

//Constructor 

Eukaryote::Eukaryote(int id, double size, string organeName): Cell(id, size), organeName(organeName){
    this->type = "Eukaryote";
}


// Getter and Setter

string Eukaryote::getOrganeName() const {
    return this->organeName;
}

list<Chromosome> Eukaryote::getChromosomeList() const {
    return this->chromoList;
}

void Eukaryote::setOrganeName(string organeName) {
    this->organeName = organeName;
}

void Eukaryote::setChromosomeList(list<Chromosome> chromoList) {
    this->chromoList = chromoList;
}


// Display

void Eukaryote::affiche(ostream &out) const {
    out << "**********************************************************************\n" << endl;
    out << " Cellules Eukaryote" << endl;
    Cell::affiche(out);
    out << " - Nom d'organes = " << this->organeName << "." << endl;
    if (!this->chromoList.empty()) {
        out << " - Liste de chromosome : " << endl;

        for (auto const &a : chromoList)
            out << "\t" << a;
    }
    out << "\n**********************************************************************" << endl;
}


// Function

void Eukaryote::addChromosome(Chromosome chromo) {
    this->chromoList.push_back(chromo);
}


// Copy Constructor

Eukaryote::Eukaryote(const Eukaryote &euka) {
    this->id = euka.id;
    this->size = euka.size;
    this->organeName = euka.organeName;
    this->chromoList = euka.chromoList;
    this->proteinList = euka.proteinList;
}


// Override operator

ostream & operator<<(ostream &out, const Eukaryote &p){
    p.affiche(out);
    return out;
}

bool Eukaryote::operator==(Eukaryote euka) const {
    return (this->id == euka.id) && (this->size == euka.size) && (this->proteinList == euka.proteinList) && (this->chromoList == euka.chromoList);

}
